import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CatalogService} from "../catalog.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  product: any;

  constructor(private route: ActivatedRoute,
              private catalogService: CatalogService) {
  }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.product = this.catalogService.getProduct(id);
  }

}
