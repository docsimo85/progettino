import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {JwPaginationModule} from "jw-angular-pagination";

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';


@NgModule({
  declarations: [
    CatalogComponent
  ],
  imports: [
    CommonModule,
    CatalogRoutingModule,
    JwPaginationModule,
  ]
})
export class CatalogModule { }
