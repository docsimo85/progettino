import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CatalogComponent} from './catalog.component';

const routes: Routes = [
  {path: '', component: CatalogComponent},
  {path: 'product/:id', loadChildren: () => import('./product/product.module').then(m => m.ProductModule)},
  { path: 'discount', loadChildren: () => import('./discounted-item/discounted-item.module').then(m => m.DiscountedItemModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule {
}
