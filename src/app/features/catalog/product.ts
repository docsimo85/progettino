export interface Product {
  id: number;
  name: string;
  imageUrl: string;
  price: number;
  salePrice?: number;
  category: string;
}
