import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DiscountedItemRoutingModule } from './discounted-item-routing.module';
import { DiscountedItemComponent } from './discounted-item.component';
import {JwPaginationModule} from "jw-angular-pagination";


@NgModule({
  declarations: [
    DiscountedItemComponent
  ],
  imports: [
    CommonModule,
    DiscountedItemRoutingModule,
    JwPaginationModule
  ]
})
export class DiscountedItemModule { }
