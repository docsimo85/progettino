import { Component, OnInit } from '@angular/core';
import {Product} from "../product";
import {CatalogService} from "../catalog.service";

@Component({
  selector: 'app-discounted-item',
  templateUrl: './discounted-item.component.html',
  styleUrls: ['./discounted-item.component.css']
})
export class DiscountedItemComponent implements OnInit {
  catalog: Product[] = [];
  pageOfItems: Array<any> = [];

  constructor(private catalogService: CatalogService) { }

  ngOnInit(): void {
    this.catalog = this.catalogService.getDiscountedProducts();
  }

  onChangePage(pageOfItems: Array<any>) {
    this.pageOfItems = pageOfItems;
  }

}
