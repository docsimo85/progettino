import {Component, OnInit} from '@angular/core';
import {CatalogService} from "./catalog.service";
import {Product} from "./product";

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {
  catalog: Product[] = [];
  pageOfItems: Array<any> = [];

  constructor(private catalogService: CatalogService) {
  }

  ngOnInit(): void {
    this.catalog = this.catalogService.getCatalog();
  }

  onChangePage(pageOfItems: Array<any>) {
    this.pageOfItems = pageOfItems;
  }

}
