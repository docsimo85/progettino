import {Injectable} from '@angular/core';
import {Product} from "./product";

@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  constructor() {
  }

  products: Product[] = [
    {
      id: 1,
      name: 'Lego Star Wars',
      price: 20,
      salePrice: 16,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 2,
      name: 'Lego Star Wars',
      price: 30,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 3,
      name: 'Lego Star Wars',
      price: 56,
      salePrice: 23,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 4,
      name: 'Lego Star Trek',
      price: 256,
      salePrice: 146,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 5,
      name: 'Lego Star Wars',
      price: 232,
      salePrice: 164,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 6,
      name: 'Lego Star Wars',
      price: 12,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 7,
      name: 'Lego Star Trek',
      price: 12,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 6575,
      name: 'Lego Star Wars',
      price: 34,
      salePrice: 3,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 5653,
      name: 'Lego Star Wars',
      price: 12,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 9856,
      name: 'Lego Star Wars',
      price: 67,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 568,
      name: 'Lego Star Trek',
      price: 12,
      salePrice: 3,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 573,
      name: 'Lego Star Wars',
      price: 12,
      salePrice: 3,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 90,
      name: 'Lego Star Wars',
      price: 87,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 98,
      name: 'Lego Star Trek',
      price: 12,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 345,
      name: 'Lego Star Wars',
      price: 90,
      salePrice: 3,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 353,
      name: 'Lego Star Wars',
      price: 12,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 22,
      name: 'Lego Star Wars',
      price: 12,
      salePrice: 3,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 7687,
      name: 'Lego Star Trek',
      price: 77,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 68,
      name: 'Lego Star Wars',
      price: 12,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 568,
      name: 'Lego Star Wars',
      price: 55,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 568,
      name: 'Lego Star Wars',
      price: 12,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 456,
      name: 'Lego Star Trek',
      price: 12,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 3564,
      name: 'Lego Star Wars',
      price: 12,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 87668,
      name: 'Lego Star Wars',
      price: 33,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 678,
      name: 'Lego Star Wars',
      price: 12,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    },
    {
      id: 3544,
      name: 'Lego Star Wars',
      price: 12,
      category: 'Lego',
      imageUrl: 'https://via.placeholder.com/350x280.png'
    }
  ]

  getCatalog() {
    return this.products
  }

  getProduct(id: number) {
    return this.products.find((product: { id: number }) => {
      return product.id == id
    });
  }

  getDiscountedProducts() {
    return this.products.filter(function(product) {
      return product.salePrice;
    });
  }
}
