import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home.component';
import {HomeCarouselModule} from "./home-carousel/home-carousel.module";
import {HomeOurServiceModule} from "./home-our-service/home-our-service.module";


@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    HomeCarouselModule,
    HomeOurServiceModule,
  ]
})
export class HomeModule {
}
