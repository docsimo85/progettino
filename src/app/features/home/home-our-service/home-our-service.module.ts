import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeOurServiceComponent } from './home-our-service.component';



@NgModule({
  declarations: [
    HomeOurServiceComponent
  ],
  exports: [
    HomeOurServiceComponent
  ],
  imports: [
    CommonModule
  ]
})
export class HomeOurServiceModule { }
