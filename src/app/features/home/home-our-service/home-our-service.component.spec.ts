import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeOurServiceComponent } from './home-our-service.component';

describe('HomeOurServiceComponent', () => {
  let component: HomeOurServiceComponent;
  let fixture: ComponentFixture<HomeOurServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeOurServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeOurServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
