import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeCarouselComponent } from './home-carousel.component';
import {CarouselModule} from "ngx-bootstrap/carousel";



@NgModule({
  declarations: [
    HomeCarouselComponent
  ],
  exports: [
    HomeCarouselComponent
  ],
  imports: [
    CommonModule,
    CarouselModule
  ]
})
export class HomeCarouselModule { }
