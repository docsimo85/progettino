import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { AlertModule } from 'ngx-bootstrap/alert';

import {ContactRoutingModule} from './contact-routing.module';
import {ContactComponent} from './contact.component';
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    ContactComponent
  ],
  imports: [
    CommonModule,
    ContactRoutingModule,
    ReactiveFormsModule,
    AlertModule
  ]
})
export class ContactModule {
}
