import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  inviato: boolean = false;

  contactForm = new FormGroup({
    name: new FormControl('', Validators.required),
    message: new FormControl('', Validators.required),
    privacy: new FormControl('', Validators.requiredTrue)
  });

  constructor() { }

  sendMessage(value: any){
    console.log('inviato');
    console.log(value);
    this.inviato = true;
  }

  ngOnInit(): void {
  }

}
